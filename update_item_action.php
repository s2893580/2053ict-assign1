<?php
/*
 * Updates item from form data.
 */
require "includes/defs.php";

# Get form data
$id = $_POST['id'];
$summary = $_POST['summary'];
$details = $_POST['details'];
$industry = $_POST['industry'];
$area = $_POST['area'];
$salary = $_POST['salary'];

# Check data is valid
if (empty($summary)) {
    $error = "Job must not be empty.";
    header("Location: update_item?id=$id&error=$error");
    exit;
}

# Perform update with data
update_item($id,$summary,$details,$industry,$area,$salary);

header("Location: item_detail.php?id=$id"); 
exit;
?>