{extends file='layout.tpl'}

{block name=body}
<body>

  <h1>Job - {$item.summary}</h1>
    
  <p>
  Details - {$item.details}
  
  <p>
  Industry - {$item.industry}

  <p>
  Location - {$item.area}
  
  <p>
  Salary - {$item.salary}
  
  <p>
  <a href="delete_item_action.php?id={$item.id}">Delete this job</a>
  
  <p>
  <a href="update_item.php?id={$item.id}">Update this job</a>
  
  <p>
  <a href="employers.php">Back</a>
</body>

{/block}