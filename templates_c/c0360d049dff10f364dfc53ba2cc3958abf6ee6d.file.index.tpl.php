<?php /* Smarty version Smarty-3.1.16, created on 2014-04-28 11:49:27
         compiled from "./templates/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1509802625535b4174b6a635-10553446%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c0360d049dff10f364dfc53ba2cc3958abf6ee6d' => 
    array (
      0 => './templates/index.tpl',
      1 => 1398685052,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1398685764,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1509802625535b4174b6a635-10553446',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_535b4174bd2327_86160474',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535b4174bd2327_86160474')) {function content_535b4174bd2327_86160474($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
  Job Search - Advertise a job
</title>

    <link rel="stylesheet" href="css/style.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
 
  </head>
  <body>
    
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Job Search</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="home.php">Home</a></li>
            <li><a href="about.php">About</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
 
    
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
            <div class="list-group">
              <a class="list-group-item" href="home.php">Home</a>
              <a class="list-group-item" href="index.php">index home page</a>
              <a class="list-group-item" href="employers.php">employers home page</a>
            </div>
        </div>
        <div class="col-sm-9">
          

<body>
<?php if ($_smarty_tpl->tpl_vars['query']->value) {?>
  <h1>Jobs for '<?php echo $_smarty_tpl->tpl_vars['query']->value;?>
'</h1>
<?php } else { ?>
  <h1>Available Jobs</h1>
<?php }?>
    
<form method="get" action="item_list_2.php">
  Search: <input type="text" name="query"> <input type="submit" value="Go">
</form>
</br>

<?php if ($_smarty_tpl->tpl_vars['query']->value) {?>
  <h1>Jobs for '<?php echo $_smarty_tpl->tpl_vars['query']->value;?>
'</h1>
<?php } else { ?>

<?php }?>
    
<?php if ($_smarty_tpl->tpl_vars['items']->value) {?>
<ul>
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
    <li><a href="item_detail_2.php?id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['summary'], ENT_QUOTES, 'UTF-8', true);?>
</a></li>
<?php } ?>
</ul>
<?php } else { ?>
<p>No Jobs found.</p>
<?php }?>


        </div>
      </div>
   </div>
     <div class="footer">
            Nicholas John Maric S2893580</p>
    </div>
  </body>
</html>
<?php }} ?>
