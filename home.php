<?php
/* 
 * Home page.
 */
date_default_timezone_set('UTC');
include 'includes/defs.php';
include '../Smarty/libs/Smarty.class.php';

$smarty = new Smarty;
$smarty->display("home.tpl");
?>