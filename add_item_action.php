<?php
/*
 * Adds new item from form data.
 */
require '../Smarty/libs/Smarty.class.php';
require "includes/defs.php";

# Get form data
$summary = $_POST['summary'];
$details = $_POST['details'];
$industry = $_POST['industry'];
$location = $_POST['area'];
$salary = $_POST['salary'];

# Check data is valid
if (empty($summary)) {
    $error = "Job must not be empty.";
    header("Location: add_item.php?error=$error");
    exit;
}

# add new item with form data
$id = add_item($summary,$details,$industry,$location,$salary);

header("Location: item_detail.php?id=$id"); 
exit;
?>