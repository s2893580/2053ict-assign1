<?php /* Smarty version Smarty-3.1.16, created on 2014-04-28 11:43:44
         compiled from "./templates/item_detail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1932434366534b81e3e04c52-66344424%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e42dead8a9632070fd5f182aa173d46480408a41' => 
    array (
      0 => './templates/item_detail.tpl',
      1 => 1398685420,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1398685324,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1932434366534b81e3e04c52-66344424',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_534b81e3e64731_47640572',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_534b81e3e64731_47640572')) {function content_534b81e3e64731_47640572($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Job Search</title>

    <link rel="stylesheet" href="css/style.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
 
  </head>
  <body>
    
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Job Search</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="home.php">Home</a></li>
            <li><a href="about.php">About</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
 
    
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
            <div class="list-group">
              <a class="list-group-item" href="home.php">Home</a>
              <a class="list-group-item" href="index.php">Find a job</a>
              <a class="list-group-item" href="employers.php">Advertise a job</a>
            </div>
        </div>
        <div class="col-sm-9">
          
<body>

  <h1>Job - <?php echo $_smarty_tpl->tpl_vars['item']->value['summary'];?>
</h1>
    
  <p>
  Details - <?php echo $_smarty_tpl->tpl_vars['item']->value['details'];?>

  
  <p>
  Industry - <?php echo $_smarty_tpl->tpl_vars['item']->value['industry'];?>


  <p>
  Location - <?php echo $_smarty_tpl->tpl_vars['item']->value['area'];?>

  
  <p>
  Salary - <?php echo $_smarty_tpl->tpl_vars['item']->value['salary'];?>

  
  <p>
  <a href="delete_item_action.php?id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
">Delete this job</a>
  
  <p>
  <a href="update_item.php?id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
">Update this job</a>
  
  <p>
  <a href="employers.php">Back</a>
</body>


        </div>
      </div>
   </div>
     <div class="footer">
            Nicholas John Maric S2893580</p>
    </div>
  </body>
</html>
<?php }} ?>
