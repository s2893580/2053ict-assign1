{extends file='layout.tpl'}

{block name=title}
  Job Discription
{/block}

{block name=body}
<body>
{if $query}
  <h1>Jobs for '{$query}'</h1>
 {else}
  <h1>Advertise Your Job</h1>
{/if}
    
{if $items}
  <ul>
  {foreach $items as $item}
      <li><a href="item_detail_2.php?id={$item.id}">{$item.summary|escape}</a></li>
  {/foreach}
  </ul>
  {else}
  <p>No jobs found.</p></br>
{/if}


  <p><a href="index.php">Back</a></p>

</body>
{/block}
