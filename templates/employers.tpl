{extends file='layout.tpl'}

{block name=title}
  Job Discription
{/block}

{block name=body}
<body>
{if $query}
  <h1>Jobs for '{$query}'</h1>
 {else}
  <h1>Advertise Your Job</h1>
{/if}
    
<p><a href="add_item.php">Add a new job</a></p></br>


{if $items}
  <ul>
  {foreach $items as $item}
      <li><a href="item_detail.php?id={$item.id}">{$item.summary|escape}</a></li>
  {/foreach}
  </ul>
  {else}
  <p>No jobs found.</p></br>
{/if}

{if $query}
  <p><a href="employers.php">Back</a></p>
{/if}
</body>
{/block}
