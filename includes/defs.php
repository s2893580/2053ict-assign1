<?php
/*
 * funtions that add, delete, update and search.
 */
require "db_defs.php";

date_default_timezone_set('UTC');
/* Adds a new item from form data and returns its id. */
function add_item($summary,$details,$industry,$area,$salary) {
    try {
        $db = db_open();
        $sql = "insert into ld_items (summary, details, industry, area, salary) " .
        "values (:summary, :details, :industry, :area, :salary)";
        $statement = $db->prepare($sql);
        $statement->bindValue(':summary', $summary);
        $statement->bindValue(':details', $details);
        $statement->bindValue(':industry', $industry);
        $statement->bindValue(':area', $area);
        $statement->bindValue(':salary', $salary);
        $statement->execute();
        $id = $db->lastInsertId();
    } catch(PDOException $e) {
        die("Error: " . $e->getMessage());
    }
    return $id;
}


/* Gets list of items that match $str, if present, in databse. */
function get_items($str) {
    try{
        $db = db_open();
        $sql = "select id, summary, details, industry, area, salary from ld_items ";
        if ($str) {
            $sql .= "where summary like :str or details like :str or industry like :str or area like :str or salary like :str";
        }
        $statement = $db->prepare($sql);
        if ($str){
            $statement->bindValue(':str', "%$str%");
        }
        $statement->execute();
      
        $items = $statement->fetchAll();
        return $items;
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }
}

/*Gets item with the given id.*/
function get_item($id) {
    try {
        $db = db_open();
        $sql = "select id, summary, details, industry, area, salary from ld_items where id = :id";
        $statement = $db->prepare($sql);
        $statement->bindValue(':id', $id);
        $statement->execute();
        $items = $statement->fetchAll();
        if (count($items) != 1){
            die("Invalid query or result: $sql\n");
        }
        return $items[0];
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }
}

/* Updates an item with the given id using the given summary and details. */
function update_item($id,$summary,$details,$industry,$area,$salary) {
      try {
        $db = db_open();
        $sql = "update ld_items " .
               "set summary = :summary, details = :details, industry = :industry, area = :area, salary = :salary " .
               "where id = :id";
        $statement = $db->prepare($sql);
        $statement->bindValue(':summary', $summary);
        $statement->bindValue(':details', $details);
        $statement->bindValue(':industry', $industry);
        $statement->bindValue(':area', $area);
        $statement->bindValue(':salary', $salary);
        $statement->bindValue(':id', $id);
        $statement->execute();
      }
  catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }

}

/* Deletes the item with the given id. */
function delete_item($id) {
        try {
        $db = db_open();
        $sql = "delete from ld_items where id = :id";
        $statement = $db->prepare($sql);
        $statement->bindValue(':id', $id);
        $statement->execute();
      }
  catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }

}