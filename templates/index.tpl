{extends file='layout.tpl'}

{block name=title}
  Job Search - Advertise a job
{/block}

{block name=body}

<body>
{if $query}
  <h1>Jobs for '{$query}'</h1>
{else}
  <h1>Available Jobs</h1>
{/if}
    
<form method="get" action="item_list_2.php">
  Search: <input type="text" name="query"> <input type="submit" value="Go">
</form>
</br>

{if $query}
  <h1>Jobs for '{$query}'</h1>
{else}

{/if}
    
{if $items}
<ul>
{foreach $items as $item}
    <li><a href="item_detail_2.php?id={$item.id}">{$item.summary|escape}</a></li>
{/foreach}
</ul>
{else}
<p>No Jobs found.</p>
{/if}

{/block}
