{extends file='layout.tpl'}

{block name=body}
<body>

  <h1>Job - {$item.summary}</h1>
    
  <p>
  Details - {$item.details}
  
  <p>
  Industry - {$item.industry}

  <p>
  Location - {$item.area}
  
  <p>
  Salary - {$item.salary}

  <p>
  <a href="index.php">Back</a>
</body>

{/block}