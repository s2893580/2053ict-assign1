<?php
/*
 * Deletes item with given id.
 */
require "includes/defs.php";

$id = $_GET['id'];

delete_item($id);

header("Location: employers.php");
exit;
?>