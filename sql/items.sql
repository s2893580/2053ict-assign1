/*
* creates inital table
*/
drop table if exists ld_items;


create table ld_items (
    id integer not null primary key,
    summary varchar(80) not null,
    details text default '',
    industry text default '',
    area text default '',
    salary text default ''
);

insert into ld_items values (null, "Space man",  "spaceman needed", "space", "space", "10-12");
insert into ld_items values (null, "Space man",  "spaceman needed", "space", "space", "10-12");
insert into ld_items values (null, "Space man",  "spaceman needed", "space", "space", "10-12");
insert into ld_items values (null, "Space man",  "spaceman needed", "space", "space", "10-12");