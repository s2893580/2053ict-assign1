{extends file='layout.tpl'}

{block name=body}
<body>
<h1>Update Content</h1>
{if $error}
    <p>{$error}</p>
{/if}

<form method="post" action="update_item_action.php">
    <input type="hidden" name="id" value="{$item.id}">
    <table>
      <tr><td>Job:</td> <td><input type="text" name="summary" value="{$item.summary}"></td></tr>
      <tr><td>Details:</td> <td><textarea name="details">{$item.details}</textarea></td></tr>
      <tr><td>Industry:</td> <td><textarea name="industry">{$item.industry}</textarea></td></tr>
      <tr><td>Location:</td> <td><textarea name="area">{$item.area}</textarea></td></tr>
      <tr><td>Salary:</td> <td><textarea name="salary">{$item.salary}</textarea></td></tr>
      <tr><td><input type="submit" value="Update item"></td></tr>
    </table>
  </form>
 </form>
</body>
{/block}
