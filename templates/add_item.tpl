{extends file='layout.tpl'}

{block name=body}
<h1>Add Job</h1>

{if $error}
    <p>{$error}</p>
{/if}

<form method="post" action="add_item_action.php">
    <table>
    <tr><td>Job:</td> <td><input type="text" name="summary"></td></tr>
    <tr><td>Details:</td> <td><textarea name="details"></textarea></td></tr>
    <tr><td>Industry:</td> <td><textarea name="industry"></textarea></td></tr>
    <tr><td>Location:</td> <td><textarea name="area"></textarea></td></tr>
    <tr><td>Salary:</td> <td><textarea name="salary"></textarea></td></tr>
    <tr><td colspan=2><input type="submit" value="Add item">
    </table>
</form>
</body>
{/block}